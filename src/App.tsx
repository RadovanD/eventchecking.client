import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './components/home/home';
import Login from './components/login/login';
import AddParticipant from './components/addParticipant/addParticipant';
import { Provider } from "mobx-react";
import { GeneralDataStore } from './stores/generalDataStore';
import  Navigation from './components/navigation/navigation';

const stores = {
  general: new GeneralDataStore()
}

const App: React.FC = () => {
  return (
    <Provider store={stores.general}>
      <div className="App">
        <Router>
          <Navigation/>
          <Route path="/login" component={Login} />
          <Route path="/addParticipant" component={AddParticipant} />
          <Route exact path="/" component={Home} />
        </Router>
      </div>
    </Provider>
  )
}

export default App;
