import React from 'react'
import { Redirect } from 'react-router';
import { Mutation } from 'react-apollo';

import { ADD_PARTICIPANT } from '../../mutations';
import { GET_PARTICIPANTS } from '../../queries';

import { observer, inject } from 'mobx-react';

@inject('store') @observer
class AddParticipant extends React.Component {
    state = {
        firstName: '',
        lastName: '',
        error: ''
    }

    addParticipant = async (mutate: any) => {
        await mutate({
            variables: this.state
        })
        .then(() => this.setState({firstName: '', lastName: ''}))
        .catch((err: any) => this.setState({error: err.message}));
    }

    handleInputChange = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        const {store}: any = this.props;
        if (store.authenticated) {
            return (
                <div>
                    <h1>Add participants</h1>
                    <form>
                        <label>First Name</label>
                        <input name="firstName" type="text" value={this.state.firstName} onChange={this.handleInputChange}/>
                        <label>Last Name</label>
                        <input name="lastName" type="text" value={this.state.lastName} onChange={this.handleInputChange}/>
                        <Mutation
                            mutation={ADD_PARTICIPANT}
                            refetchQueries={() => [{ query: GET_PARTICIPANTS }]}
                        >
                            {(mutate: any) => (
                                <input type="button" value="Add" onClick={() => this.addParticipant(mutate)}/>
                            )}
                        </Mutation>
                        <p>{this.state.error}</p>
                    </form>
                </div>
            )
        } else {
            return <Redirect exact to="/login" />
        }
    }
}
export default AddParticipant