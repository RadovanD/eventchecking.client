import React from 'react'
import { Redirect } from 'react-router';
import { Query, Mutation } from 'react-apollo';
import { CHECK_IN, CHECK_OUT } from '../../mutations';
import { GET_PARTICIPANTS } from '../../queries';

import { observer, inject } from 'mobx-react';

@inject('store') 
@observer
class Home extends React.Component {

    state = {
        loading: false,
        error: ''
    };

    checkInOut = async (mutate: any, participantId: string) => {
        await mutate({
            variables: {
                id: participantId
            }
        })
        .catch((err: any) => this.setState({ error: err.message }));
    }    

    render() {
        const {store}: any = this.props;
        if ( store.authenticated ) {
            return <div>
                <h1>Home</h1>
                <table >
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Checked In?</th>
                            <th>Check in</th>
                            <th>Check out</th>
                        </tr>
                    </thead>
                    <tbody>
                        <Query query={GET_PARTICIPANTS}>
                            {({ loading, error, data }: any) => {
                                if (loading) return (<tr></tr>);
                                if (error) return (<tr></tr>);


                                return data.participants.map((participant: any, index: number) => {
                                    return (
                                        <tr key={index}>
                                            <td>{participant.firstName}</td>
                                            <td>{participant.lastName}</td>
                                            <td>{participant.checkedIn ? 'Yes' : 'No'}</td>
                                            <td>
                                                <Mutation
                                                    mutation={CHECK_IN}
                                                    refetchQueries={() => [{ query: GET_PARTICIPANTS }]}
                                                >
                                                    {(mutate: any) => (
                                                        <input type="button" value="Check in" onClick={() => this.checkInOut(mutate, participant._id)} />
                                                    )}
                                                </Mutation>
                                            </td>
                                            <td>
                                                <Mutation
                                                    mutation={CHECK_OUT}
                                                    refetchQueries={() => [{ query: GET_PARTICIPANTS }]}
                                                >
                                                    {(mutate: any) => (
                                                        <input type="button" value="Check out" onClick={() => this.checkInOut(mutate, participant._id)} />
                                                    )}
                                                </Mutation>
                                            </td>
                                        </tr>
                                    )
                                })


                            }}
                        </Query>
                    </tbody>
                </table>

            </div>
        } else {
            return <Redirect exact to="/login" />
        }
    }
}
export default Home