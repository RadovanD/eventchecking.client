import React from 'react'

import { Mutation } from "react-apollo";
import { Redirect } from 'react-router';

import { LOGIN } from '../../mutations';

import { observer, inject } from 'mobx-react';

@inject('store') @observer
class Login extends React.Component {
    state = {
        username: '',
        password: '',
        error: ''
    }

    handleSubmit = async (mutate: any) => {
        await mutate({
            variables: this.state
        })
        .then((result: any) => {
            localStorage.setItem("token", result.data.login);

            const {store}: any = this.props;
            store.setAuthenticated(true);
        })
        .catch((err: any) => this.setState({ error: err.message }));
    }

    handleChange = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        const {store}: any = this.props;
        if (store.authenticated) {
            return <Redirect exact to="/" />
        } else {
            return (
                <Mutation
                    mutation={LOGIN}
                >{(mutate: any) => (
                    <div>
                        <h1>Login</h1>
                        <form>
                            <label htmlFor="username">Username:</label>
                            <input name="username" type="text" value={this.state.username} onChange={this.handleChange} />

                            <label htmlFor="password">Password:</label>
                            <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />

                            <input value="Login" type="button" onClick={() => this.handleSubmit(mutate)} />
                        </form>
                        <p>{this.state.error}</p>
                    </div>
                )}</Mutation>
            )
        }

    }
}
export default Login