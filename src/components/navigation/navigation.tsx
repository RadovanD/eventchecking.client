import React from 'react'
import { Link } from 'react-router-dom';
import {observer, inject} from 'mobx-react';

@inject('store') @observer
class Navigation extends React.Component {
    render() {
        const {store}: any = this.props;
        return (
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/addParticipant">Add Participant</Link>
                        </li>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            {!store.authenticated ? <Link to="/login">Login</Link> : <Link to="/login" onClick={() => store.setAuthenticated(false)}>Logout</Link>}
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Navigation