import gql from 'graphql-tag';

export const ADD_PARTICIPANT = gql`
    mutation AddParticipant($firstName:String!, $lastName:String!) {
        addParticipant(firstName:$firstName, lastName: $lastName) {
            firstName,
            lastName
        }
    }
`;

export const CHECK_IN = gql`
    mutation CheckIn($id:String!) {
        checkIn(id:$id){
            checkedIn,
            firstName,
            lastName
        }
    }
`;

export const CHECK_OUT = gql`
    mutation CheckOut($id:String!) {
        checkOut(id:$id){
            checkedIn,
            firstName,
            lastName
        }
    }
`;

export const LOGIN = gql`
    mutation Login($username:String!, $password: String!) {
        login(username: $username, password:$password)
    } 
`;