import gql from 'graphql-tag';

export const GET_PARTICIPANTS = gql`
    {
        participants {
            _id,
            firstName,
            lastName,
            checkedIn
        }
    }
`;