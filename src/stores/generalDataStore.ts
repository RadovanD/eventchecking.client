import { observable, action } from "mobx";
export class GeneralDataStore {
    @observable authenticated: boolean = !!localStorage.getItem('token');

    @action setAuthenticated(value: boolean) {
        if(value === false) localStorage.removeItem('token');
        this.authenticated = value;
    }
}